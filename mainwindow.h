#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_mainwindow.h"

#define ROJO 0
#define VERDE 1

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Ui::MainWindow *ui;
    pthread_t thread;
    bool Playing;
    bool Stoped;
    int fd;
    bool Sem_Thread;
    void Error (void);
    void PlaySong   (void);
    void PauseSong  (void);

public slots:
    void PlayButton (void);
    void StopButton (void);
    void NextButton (void);
    void PrevButton (void);
    void UartConnect(void);
    void UartDisConnect(void);
    void ChangeSelected (QListWidgetItem*);
private:
    int ReadUART (char*str,int cant);
    int SendUART (char*str,int cant);
    int Fill_List(void);

};

void* UART_Thread (void* Arg);
#endif // MAINWINDOW_H

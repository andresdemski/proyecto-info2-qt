#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <QMessageBox>
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <termios.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Playing=0;
    Stoped=1;
    fd=0;
    Sem_Thread= ROJO;
    ui->FileList->setCurrentRow(0);

    connect(ui->PB_Connect,SIGNAL(clicked(bool)),this,SLOT(UartConnect()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::PlayButton (void)
{
    if (SendUART((char*)"&p.\n",4) == -1)
    {
        Error();
        return;
    }
    if (Playing) PauseSong();
    else
    {
        PlaySong();
        if (Stoped)
        {
            Stoped=0;
            ui->SongName->setText(ui->FileList->currentItem()->text());
        }
    }
}

void MainWindow::StopButton (void)
{
    if (SendUART((char*)"&x.\n",4) == -1)
    {
        Error();
        return;
    }
    ui->SongName->clear();
    ui->PB_Play->setText("PLAY");
    Playing=0;
    Stoped=1;
}

void MainWindow::NextButton (void)
{
   if (SendUART((char*)"&n.\n",4) == -1)
   {
       Error();
       return;
   }
   //disconnect(ui->FileList,SIGNAL(currentRowChanged(int)),this,SLOT(ChangeSelected(int)));
   ui->FileList->setCurrentRow(ui->FileList->currentRow()+1);
   if (ui->FileList->currentRow() < 0) ui->FileList->setCurrentRow(0);
  // connect(ui->FileList,SIGNAL(currentRowChanged(int)),this,SLOT(ChangeSelected(int)));
}

void MainWindow::PrevButton (void)
{
    if (SendUART((char*)"&b.\n",4) == -1)
    {
        Error();
        return;
    }
   // disconnect(ui->FileList,SIGNAL(currentRowChanged(int)),this,SLOT(ChangeSelected(int)));
    ui->FileList->setCurrentRow(ui->FileList->currentRow()-1);
    if (ui->FileList->currentRow() < 0) ui->FileList->setCurrentRow(ui->FileList->count() -1);
    //connect(ui->FileList,SIGNAL(currentRowChanged(int)),this,SLOT(ChangeSelected(int)));
}

void MainWindow::UartDisConnect()
{
    ::close(fd);
    fd=0;

    disconnect(ui->PB_Connect,SIGNAL(clicked(bool)),this,SLOT(UartConnect()));
    disconnect(ui->FileList,SIGNAL(itemClicked(QListWidgetItem*)),this,SLOT(ChangeSelected(QListWidgetItem*)));
    disconnect(ui->PB_Play,SIGNAL(clicked(bool)),this,SLOT(PlayButton()));
    disconnect(ui->PB_Stop,SIGNAL(clicked(bool)),this,SLOT(StopButton()));
    disconnect(ui->PB_Prev,SIGNAL(clicked(bool)),this,SLOT(PrevButton()));
    disconnect(ui->PB_Next,SIGNAL(clicked(bool)),this,SLOT(NextButton()));
    disconnect(ui->PB_Connect,SIGNAL(clicked(bool)),this,SLOT(UartDisConnect()));

    pthread_cancel(thread);
    ui->PB_Connect->setText("Connect");
    ui->FileList->clear();
    ui->SongName->clear();

    connect(ui->PB_Connect,SIGNAL(clicked(bool)),this,SLOT(UartConnect()));
    return;
}

void MainWindow::UartConnect()
{
    struct termios Serial;
    system ("gksudo chmod 666 /dev/ttyUSB0");

    if ((fd = open("/dev/ttyUSB0",O_RDWR) )<0)
    {
        Error();
        return;
    }

    else
    {
        cfsetispeed(&Serial, B115200);
        tcgetattr(fd, &Serial);

        Serial.c_iflag = 0;
        Serial.c_oflag = 0;
        Serial.c_lflag = 0;
        Serial.c_cc[VMIN] = 1;//works for 1
        Serial.c_cc[VTIME] = 1;
        tcsetattr(fd, TCSANOW, &Serial); //Set newly-modified attributes

        if (Fill_List() == -1)
        {
            Error ();
            return;
        }

        Stoped=1;
        Playing=0;

        pthread_create(&thread,NULL,UART_Thread,this);

        ui->PB_Connect->setText("Disconnect");

        ui->PB_Play->setText("PLAY");
        connect(ui->PB_Play,SIGNAL(clicked(bool)),this,SLOT(PlayButton()));
        connect(ui->PB_Stop,SIGNAL(clicked(bool)),this,SLOT(StopButton()));
        connect(ui->PB_Prev,SIGNAL(clicked(bool)),this,SLOT(PrevButton()));
        connect(ui->PB_Next,SIGNAL(clicked(bool)),this,SLOT(NextButton()));
        connect(ui->FileList,SIGNAL(itemClicked(QListWidgetItem*)),this,SLOT(ChangeSelected(QListWidgetItem*)));
        disconnect(ui->PB_Connect,SIGNAL(clicked(bool)),this,SLOT(UartConnect()));
        connect(ui->PB_Connect,SIGNAL(clicked(bool)),this,SLOT(UartDisConnect()));

    }
}


void MainWindow::PlaySong()
{
    ui->PB_Play->setText("PAUSE");
    Playing=1;
}

void MainWindow::PauseSong ()
{
    ui->PB_Play->setText("PLAY");
    Playing=0;
}

void MainWindow::ChangeSelected (QListWidgetItem* selected)
{
    char buff[7];
    sprintf (buff,"&s.%d\n",selected->listWidget()->currentRow());
    if (SendUART (buff,strlen(buff)) == -1 )
    {
        Error();
        return;
    }
}

int MainWindow::Fill_List()
{
    char buff[30];
    char aux[3];
    int i,sel;
    char name[30];

    SendUART ((char*)"&c.\n",4);
    while(1)
    {
        if (ReadUART(aux,3) == -1) return -1;
        if (aux[0]=='&' && aux[2]=='.')
        {
            switch (aux[1])
            {
            case 'f':
                i=0;
                do
                {
                    if (ReadUART(aux,1) != -1)
                    {
                        name[i]=aux[0];
                        i++;
                    }
                }while (aux[0] != '\n');

                name[i-1]=0;
                ui->FileList->addItem(name);
                break;

            case 's':
                i=0;
                do
                {
                    if (ReadUART(aux,1) == -1) return -1;
                    buff[i]=aux[0];
                    i++;
                }while (aux[0] != '\n');
                buff[i-1]=0;
                sel= atoi (buff);
                ui->FileList->setCurrentRow(sel);
                break;

            case 'x':
                if (ReadUART(aux,1) == -1) return -1;
                return 0;
                break;
            }
        }
    }

    return 0;
}

int MainWindow:: ReadUART (char* str ,int cant)
{

    int retval;
    fd_set rfds, master;
    struct timeval timeout;  //para el time out

    FD_ZERO(&master);
    FD_SET(fd, &master); //socket
    timeout.tv_sec = 0;
    timeout.tv_usec = 1000000;

    rfds = master;
    retval = select(fd + 1, &rfds, NULL, NULL, &timeout);
    if (retval>0)
    {
        if (read (fd, str,cant) == cant )
        {
            return 0;
        }
    }
    str[0]=0;
    return -1;
}

int MainWindow::SendUART (char* str,int cant)
{
    if (write (fd,str,cant) != cant) return -1;
    return 0;
}

void MainWindow:: Error()
{

    UartDisConnect();
    QMessageBox msgBox;
    msgBox.warning(this,"ERROR","Error en la coneccion con el dispositivo");
}

void* UART_Thread (void* arg)
{
    MainWindow *Obj = (MainWindow*)arg;
    char buff[17],aux;
    int retval,i;
    fd_set rfds, master;

    Obj->Sem_Thread=VERDE;

    FD_ZERO(&master);
    FD_SET(Obj->fd, &master);

    while (1)
    {
        rfds = master;
        retval = select(Obj->fd + 1, &rfds, NULL, NULL, NULL);
        if (retval>0)
        {
            if (Obj->Sem_Thread == VERDE)
            {
                if (read (Obj->fd, buff,3) != 3 ) //Por lo menos tienen 3 bites: &x.
                {
                    //Obj->Error();
                    return 0;
                }

                if (buff[1] != 's')  //s es el unico que contiene mas que un \n
                {
                    if (read (Obj->fd, &aux,1) < 0 )
                    {
                       // Obj->Error();
                        return 0;
                    }

                }

                switch (buff[1])
                {
                case 'p':
                    if (Obj->Playing) Obj->PauseSong();
                    else
                    {
                        Obj->PlaySong();
                        if (Obj->Stoped)
                        {
                            Obj->Stoped=0;
                            Obj->ui->SongName->setText(Obj->ui->FileList->currentItem()->text());
                        }
                    }
                    break;

                case 's':
                    i=0;
                    do
                    {
                        if (read (Obj->fd, &aux,1) < 0 )
                        {
                           // Obj->Error();
                            return 0;
                        }
                        buff[i]=aux;
                        i++;
                    }while (aux != '\n');
                    buff[i-1]=0;

                    //MainWindow::disconnect(Obj->ui->FileList,SIGNAL(currentRowChanged(int)),Obj,SLOT(ChangeSelected(int)));
                    Obj->ui->FileList->setCurrentRow(atoi (buff));
                    //MainWindow::connect(Obj->ui->FileList,SIGNAL(currentRowChanged(int)),Obj,SLOT(ChangeSelected(int)));
                    break;

                case 'b':
                    //MainWindow::disconnect(Obj->ui->FileList,SIGNAL(currentRowChanged(int)),Obj,SLOT(ChangeSelected(int)));
                    Obj->ui->FileList->setCurrentRow(Obj->ui->FileList->currentRow()-1);
                    if (Obj->ui->FileList->currentRow() < 0) Obj->ui->FileList->setCurrentRow(Obj->ui->FileList->count() -1);
                    //MainWindow::connect(Obj->ui->FileList,SIGNAL(currentRowChanged(int)),Obj,SLOT(ChangeSelected(int)));
                    break;

                case 'n':
                    //MainWindow::disconnect(Obj->ui->FileList,SIGNAL(currentRowChanged(int)),Obj,SLOT(ChangeSelected(int)));
                    Obj->ui->FileList->setCurrentRow(Obj->ui->FileList->currentRow()+1);
                    if (Obj->ui->FileList->currentRow() < 0) Obj->ui->FileList->setCurrentRow(0);
                    //MainWindow::connect(Obj->ui->FileList,SIGNAL(currentRowChanged(int)),Obj,SLOT(ChangeSelected(int)));
                    break;

                case 'x':
                    Obj->ui->SongName->clear();
                    Obj->ui->PB_Play->setText("PLAY");
                    Obj->Playing=0;
                    Obj->Stoped=1;
                    break;
                }

            }
        }
        else
        {
            //Obj->Error();
            return 0;
        }
    }
    return 0;
}
